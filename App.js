import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { Dimensions, SafeAreaView, StyleSheet, Platform } from "react-native";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import authReducer from "./store/reducers/authReducer";
import ReduxThunk from "redux-thunk";
import AppLoading from "expo-app-loading";
import Navigation from "./navigation";
import * as Font from "expo-font";
import {
  useFonts,
  Montserrat_400Regular,
  Montserrat_500Medium,
  Montserrat_600SemiBold,
  Montserrat_700Bold,
  Montserrat_800ExtraBold,
  Montserrat_900Black,
} from "@expo-google-fonts/montserrat";

const rootReducer = combineReducers({
  auth: authReducer,
});
const composeEnhancers =
  (typeof window !== "undefined" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(ReduxThunk))
);

export default function App() {
  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
    Montserrat_900Black,
  });
  if (!fontsLoaded) {
    return <AppLoading onError={console.warn} />;
  }
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        <Navigation />
        <StatusBar style="auto" />
      </SafeAreaView>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: Dimensions.get("window").height,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});
