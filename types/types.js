export const types = {
  login: "[Auth] Login",
  logout: "[Auth] Logout",
  // AUTHENTICATION
  authCheckingFinish: "[Auth] Finish Checking login state",
  authStartLogin: "[Auth] Start Login",
  authLogin: "[Auth] Login",
  authStartRegister: "[Auth] Start register",
  authStartTokenRenew: "[Auth] Start token renew",
  authLogout: "[Auth] Logout",
};
