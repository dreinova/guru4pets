import { fetchSinToken } from "../../helpers/fetch";
import { types } from "../../types/types";

export const startLogin = (email, password, phone) => {
  return async (dispatch) => {
    dispatch(startLoading());
    const resp = await fetchSinToken(
      "acuarelausers/login",
      { mail: email, pass: password, phone: phone },
      "POST"
    );
    const body = await resp.json();
    console.log(body);
  };
};

const login = (user) => ({
  type: types.login,
  user,
});
