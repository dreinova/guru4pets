import { types } from "../../types/types";
export const initialState = {
  isLoggedIn: false,
  user: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.login:
      let { user } = action;
      return { ...state, isLoggedIn: true, user };
    case types.logout:
      return { ...state, ...initialState };
    case types.authCheckingFinish:
      return {
        ...state,
        logged: false,
        checking: false,
      };

    default:
      return state;
  }
};
