import React from "react";
import { Pressable, View } from "react-native";

export const TouchableCmp = (props) => {
  return (
    <View style={[props.style]}>
      <Pressable
        onPress={() => {
          props.onPress();
        }}
      >
        {props.children}
      </Pressable>
    </View>
  );
};
