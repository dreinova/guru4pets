import React from "react";
import styles from "./styles";
import { View, Text, TextInput } from "react-native";

export const Search = (props) => {
  return (
    <View
      style={{
        backgroundColor: "#EEEEEE",
        paddingVertical: 20,
        paddingHorizontal: 22,
      }}
    >
      <Text
        style={{
          fontFamily: "Montserrat_700Bold",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: 22,
          lineHeight: 27,
          color: "#333",
          marginBottom: 15,
        }}
      >
        ¿Qué estas buscando?
      </Text>
      <TextInput
        style={{
          paddingVertical: 10,
          paddingHorizontal: 25,
          backgroundColor: "#FFFFFF",
          borderColor: "#CCCCCC",
          borderWidth: 1,
          borderRadius: 50,
        }}
        placeholder="Bogota, Cedritos"
      />
    </View>
  );
};

export default Search;
