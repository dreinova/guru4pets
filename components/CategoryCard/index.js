import React from "react";
import styles from "./styles";
import { View, Text, Image, Dimensions } from "react-native";
import { TouchableCmp } from "../TouchableCmp";

export const CategoryCard = (props) => {
  return (
    <View
      style={{
        maxWidth: 160,
        width: Dimensions.get("window").width / 2 - 30,
        marginHorizontal: 5,
      }}
    >
      <TouchableCmp onPress={props.onPress}>
        <Image
          style={{
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            height: 150,
          }}
          source={{
            uri: props.photo,
          }}
        />
      </TouchableCmp>
      <View
        style={{
          backgroundColor: "#FCE3CD",
          padding: 15,
          paddingBottom: 18,
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
          flex: 1,
        }}
      >
        <Text
          style={{
            color: "#A64E02",
            fontFamily: "Montserrat_700Bold",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 16,
            lineHeight: 20,
          }}
        >
          {props.name}
        </Text>
      </View>
    </View>
  );
};

export default CategoryCard;
