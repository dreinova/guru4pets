import React from "react";
import styles from "./styles";
import { View, Text } from "react-native";
import IconSvg from "../IconSvg";
import { TouchableCmp } from "../TouchableCmp";

export const CategorieCircle = (props) => {
  const { name, icon, onPress } = props;
  return (
    <TouchableCmp onPress={onPress}>
      <View
        style={{
          width: 100,
          alignItems: "center",
          marginBottom: 30,
        }}
      >
        <View
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#FCE3CD",
            marginBottom: 5,
          }}
        >
          <IconSvg width="38" height="38" icon={icon} />
        </View>
        <Text
          style={{
            fontFamily: "Montserrat_400Regular",
            fontStyle: "normal",
            fontWeight: "normal",
            fontSize: 16,
            lineHeight: 20,
            textAlign: "center",
            color: "#333333",
          }}
        >
          {name}
        </Text>
      </View>
    </TouchableCmp>
  );
};

export default CategorieCircle;
