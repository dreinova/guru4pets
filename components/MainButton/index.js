import React from "react";
import styles from "./styles";
import { View, Text } from "react-native";
import { TouchableCmp } from "../TouchableCmp";
import IconSvg from "../IconSvg";

export const MainButton = (props) => {
  const { buttonTxt, secondary, icon, iconSvg } = props;
  return (
    <TouchableCmp
      onPress={() => {
        props.onPress();
      }}
    >
      <View
        style={[
          props.style,

          {
            backgroundColor: "#F27405",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 25,
            height: 40,
            alignSelf: "center",
            flexDirection: "row",
          },
          secondary && {
            backgroundColor: "transparent",
            borderWidth: 1,
            borderColor: "#F27405",
            color: "#333333",
          },
        ]}
      >
        {icon && (
          <IconSvg
            style={{ marginRight: 5 }}
            width={20}
            height={20}
            icon={iconSvg}
          />
        )}
        <Text
          style={[
            {
              fontSize: 16,
              fontStyle: "normal",
              fontWeight: "bold",
              lineHeight: 20,
              color: "#FFF",
            },
            secondary && {
              color: "#333333",
            },
          ]}
        >
          {buttonTxt}
        </Text>
      </View>
    </TouchableCmp>
  );
};

export default MainButton;
