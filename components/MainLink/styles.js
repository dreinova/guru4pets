export default {
  link: {
    fontFamily: "Montserrat_400Regular",
    color: "#1184F2",
    fontSize: 12,
    fontStyle: "normal",
    fontWeight: "normal",
    lineHeight: 15,
    marginBottom: 20,
    textAlign: "right",
    textDecorationLine: "underline",
  },
};
