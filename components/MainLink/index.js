import React from "react";
import styles from "./styles";
import { View, Text, TouchableWithoutFeedback } from "react-native";

export const MainLink = (props) => {
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        props.onPress();
      }}
    >
      <Text style={[props.style, styles.link]}>{props.children}</Text>
    </TouchableWithoutFeedback>
  );
};

export default MainLink;
