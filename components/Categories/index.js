import React, { useEffect, useState } from "react";
import styles from "./styles";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import CategorieCircle from "../CategorieCircle";
import { CategoriesList } from "../../data/CategoriesList";
import { TouchableCmp } from "../TouchableCmp";
import { useNavigation } from "@react-navigation/native";
const { width } = Dimensions.get("window");
export const Categories = (props) => {
  const navigation = useNavigation();
  const [viewAll, setViewAll] = useState(false);
  return (
    <View>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 22,
        }}
      >
        <Text
          style={{
            fontFamily: "Montserrat_700Bold",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 22,
            lineHeight: 27,
            color: "#333",
          }}
        >
          Categorías
        </Text>
        <TouchableCmp onPress={() => setViewAll(!viewAll)}>
          <Text
            style={{
              fontFamily: "Montserrat_400Regular",
              fontStyle: "normal",
              fontWeight: "normal",
              fontSize: 12,
              lineHeight: 15,
              color: "#1184F2",
              textDecorationLine: "underline",
            }}
          >
            {viewAll ? "Ver menos" : "Ver todas"}
          </Text>
        </TouchableCmp>
      </View>

      <FlatList
        scrollEnabled={viewAll}
        style={{ height: 200 }}
        contentContainerStyle={{ justifyContent: "space-between" }}
        numColumns={3}
        data={CategoriesList}
        renderItem={({ item }) => (
          <CategorieCircle
            name={item.name}
            icon={item.icon}
            onPress={() =>
              navigation.navigate("Categorie", {
                id: item.id,
                name: item.name,
              })
            }
          />
        )}
      />
    </View>
  );
};

export default Categories;
