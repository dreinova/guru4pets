import React from "react";
import styles from "./styles";
import ModalDropdown from "react-native-modal-dropdown";
import IconSvg from "../IconSvg";

export const Dropdown = (props) => {
  return (
    <ModalDropdown
      options={props.options}
      style={[styles.input, props.style]}
      textStyle={styles.label}
      onSelect={props.onSelect}
      renderButtonText={props.renderButtonText}
      dropdownTextStyle={{
        fontFamily: "Montserrat_400Regular",
        color: "#1B1B1B",
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "normal",
        lineHeight: 20,
      }}
      dropdownStyle={{
        width: 270,
      }}
      defaultValue={props.defaultValue}
      renderRightComponent={() => (
        <IconSvg
          style={{
            right: 0,
            position: "absolute",
          }}
          width="14"
          height="8"
          icon={`<svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.99996 7.99018C6.74905 7.99018 6.49818 7.89438 6.30688 7.70318L0.287198 1.68344C-0.0957326 1.30051 -0.0957326 0.679653 0.287198 0.296878C0.669973 -0.0858977 1.29071 -0.0858977 1.67367 0.296878L6.99996 5.62348L12.3263 0.297064C12.7092 -0.0857116 13.3299 -0.0857116 13.7126 0.297064C14.0957 0.679839 14.0957 1.30069 13.7126 1.68362L7.69304 7.70336C7.50165 7.8946 7.25078 7.99018 6.99996 7.99018Z" fill="#F27405"/></svg>`}
        />
      )}
      renderRow={props.renderRow}
    />
  );
};

export default Dropdown;
