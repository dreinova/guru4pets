export default {
  input: {
    borderBottomColor: "#666666",
    borderBottomWidth: 1,
    flexDirection: "row",
    padding: 8,
    justifyContent: "space-between",
    width: 270,
  },
  label: {
    width: 250,
    fontFamily: "Montserrat_700Bold",
    color: "#666666",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "bold",
    lineHeight: 20,
  },
};
