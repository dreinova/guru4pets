import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../screens/Auth/LoginScreen";
import RegisterScreen from "../screens/Auth/RegisterScreen";
import RecoverScreen from "../screens/Auth/RecoverScreen";
import SelectCity from "../screens/SelectCity";
import HomeScreen from "../screens/HomeScreen";
import CategorieScreen from "../screens/CategorieScreen";
import InternScreen from "../screens/InternScreen";

export default function Navigation() {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}

const Stack = createStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName="Login"
      gestureDirection="vertical"
    >
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Register" component={RegisterScreen} />
      <Stack.Screen name="Recovery" component={RecoverScreen} />
      <Stack.Screen name="SelectCity" component={SelectCity} />
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Categorie" component={CategorieScreen} />
      <Stack.Screen name="Intern" component={InternScreen} />
    </Stack.Navigator>
  );
}
