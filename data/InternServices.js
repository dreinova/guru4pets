export const InternServices = [
  {
    id: "1",
    name: "Peluquería expres",
    description:
      "Incluye: baño antipulgas, peinado y secado a maquina, corte de uñas y loción.",
    price: 80000,
  },
  {
    id: "2",
    name: "Peluquería VIP",
    description:
      "Incluye: baño antipulgas, peinado y secado a maquina, corte o limado de uñas, limpieza de oidos, limpieza de grandulas anales, valoración rutinaria, galletas.",
    price: 120000,
  },
  {
    id: "3",
    name: "Baño + corte de pelo + corte de uñas ",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eleifend euismod nunc a mollis. ",
    price: 30000,
  },
  {
    id: "4",
    name: "Limpieza de glandulas",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    price: 40000,
  },
  {
    id: "5",
    name: "Baño y cepillado",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    price: 30000,
  },
];
