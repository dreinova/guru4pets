export const Comments = [
  {
    id: "1",
    comment: "“Atención rápida y profesional”",
    users: "Juan Pablo Gutierrez",
    starts: 5,
  },
  {
    id: "2",
    comment: "“Cumple con los protocolos de Bioseguridad”",
    users: "Manuela Perez",
    starts: 5,
  },
  {
    id: "3",
    comment: "“Atendieron muy bien a mis perritos y los dejaron hermosos”",
    users: "Erika Ramirez",
    starts: 5,
  },
];
