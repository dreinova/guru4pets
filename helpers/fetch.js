import AsyncStorage from "@react-native-async-storage/async-storage";

const baseUrl = "https://acuarela.app/api";

const fetchSinToken = (endpoint, data, method = "GET") => {
  const url = `${baseUrl}/${endpoint}`;
  if (method === "GET") {
    return fetch(url);
  } else {
    return fetch(url, {
      method,
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(data),
    });
  }
};
const fetchToken = async (endpoint, data, method = "GET") => {
  const url = `${baseUrl}/${endpoint}`;
  const token = await AsyncStorage.getItem("user_token");
  if (method === "GET") {
    return fetch(url, {
      method,
      headers: {
        token: token,
      },
    });
  } else {
    return fetch(url, {
      method,
      headers: {
        "Content-type": "application/json",
        token: token,
      },
      body: JSON.stringify(data),
    });
  }
};

export { fetchSinToken, fetchToken };
