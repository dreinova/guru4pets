export const ModalStyles = {
  overlay: {
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
    backgroundColor: "rgba(0,0,0,.5)",
    zIndex: 10,
    height: "100%",
  },
  centeredView: {
    backgroundColor: "#FBFCFE",
    borderRadius: 16,
    marginTop: 22,
    overflow: "hidden",
    paddingHorizontal: 72,
    paddingVertical: 60,
    shadowColor: "#000",
    width: 618,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 5,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  modalTitle: {
    fontFamily: "ProximaNova-bold",
    color: "#140A4C",
    fontSize: 24,
    fontStyle: "normal",
    fontWeight: "bold",
    lineHeight: 29,
  },
  supizq: {
    bottom: -50,
    position: "absolute",
    right: -50,
  },
  actionsContainer: {
    flexDirection: "row",
    marginTop: 40,
    justifyContent: "space-between",
  },
  modalBtn: {
    width: 180,
    paddingVertical: 12,
    backgroundColor: "#FBFCFE",
    borderWidth: 2,
    borderColor: "#E1E4E9",
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
  },
  modalBtnTxt: {
    fontFamily: "ProximaNova-bold",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 12,
    lineHeight: 15,
    textAlign: "center",
    color: "#0CB5C3",
  },
  steps: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: 62,
  },
  stepCircle: {
    width: 8,
    height: 8,
    borderRadius: 8 / 2,
    backgroundColor: "#0CB5C3",
    opacity: 0.3,
  },
  stepCircleActive: {
    opacity: 1,
  },
};
