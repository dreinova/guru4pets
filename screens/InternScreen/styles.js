export default {
  image: {
    flex: 1,
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  main: {
    flex: 1,
  },
  mask: {
    backgroundColor: "transparent",
    width: "100%",
    marginBottom: 16,
    alignItems: "center",
    justifyContent: "center",
  },
};
