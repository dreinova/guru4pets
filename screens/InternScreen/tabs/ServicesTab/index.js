import React from "react";
import styles from "./styles";
import { View, Text, FlatList } from "react-native";
import { InternServices } from "../../../../data/InternServices";

export const ServicesTab = (props) => {
  return (
    <View>
      <FlatList
        data={InternServices}
        renderItem={({ item }) => (
          <View
            style={{
              paddingVertical: 20,
              borderBottomColor: "#FCE3CD",
              borderBottomWidth: 1,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: "Montserrat_700Bold",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 18,
                  lineHeight: 22,
                  color: "#1B1B1B",
                  maxWidth: 226,
                }}
              >
                {item.name}
              </Text>

              <Text
                style={{
                  fontFamily: "Montserrat_700Bold",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 14,
                  lineHeight: 17,
                  color: "#F27405",
                }}
              >
                {item.price}
              </Text>
            </View>
            <Text
              style={{
                fontFamily: "Montserrat_400Regular",
                fontStyle: "normal",
                fontWeight: "normal",
                fontSize: 16,
                lineHeight: 20,
                color: "#333",
              }}
            >
              {item.description}
            </Text>
          </View>
        )}
      />
    </View>
  );
};

export default ServicesTab;
