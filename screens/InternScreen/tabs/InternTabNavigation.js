import * as React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import ServicesTab from "./ServicesTab";
import CommentsTab from "./CommentsTab";

const Tab = createMaterialTopTabNavigator();

export function InternTabNavigation(props) {
  return (
    <Tab.Navigator
      initialRouteName="InformaciónAdicional"
      tabBarOptions={{
        activeTintColor: "rgba(166, 78, 2, 1)",
        indicatorStyle: {
          backgroundColor: "#A64E02",
          height: 4,
          borderRadius: 25,
        },
        labelStyle: {
          fontFamily: "Montserrat_700Bold",
          fontStyle: "normal",
          fontWeight: "bold",
          fontSize: 20,
          lineHeight: 24,
          textTransform: "capitalize",
        },
        inactiveTintColor: "rgba(166, 78, 2, .3)",
        style: {
          backgroundColor: "#FCE3CD",
        },
      }}
      sceneContainerStyle={{
        backgroundColor: "transparent",
        paddingHorizontal: 25,
        paddingBottom: 30,
      }}
    >
      <Tab.Screen
        name="services"
        options={{ tabBarLabel: "Servicios" }}
        component={ServicesTab}
      />
      <Tab.Screen
        name="comments"
        options={{ tabBarLabel: "Comentarios" }}
        component={CommentsTab}
      />
    </Tab.Navigator>
  );
}
